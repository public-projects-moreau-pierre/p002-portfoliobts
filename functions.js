// FONTION BOUTONS DU HEADER
const navApropos = document.querySelector('.navApropos');
const navProjets = document.querySelector('.navProjets');
const navComp = document.querySelector('.navComp');

window.addEventListener('scroll', () => {
    if (window.scrollY > 479 & window.scrollY < 1117 ) {
        navApropos.classList.add('scroll');
    } else {
        navApropos.classList.remove('scroll');
    } 

    if (window.scrollY > 1117 & window.scrollY < 2237 ) {
        navProjets.classList.add('scroll');
    } else {
        navProjets.classList.remove('scroll');
    } 

    if (window.scrollY > 2237) {
        navComp.classList.add('scroll');
    } else {
        navComp.classList.remove('scroll');
    }

})




// FONCTION D'ANIMATION STYLE "machine à écrire"
const txtAnim = document.querySelector('h1');

new Typewriter(txtAnim, {
    
    deleteSpeed: 20
})

.changeDelay(100)
.typeString('Bienvenue')
.pauseFor(1000)
.deleteChars(9)

.typeString('Welcome')
.pauseFor(1000)
.deleteChars(7)

.typeString('Bienvenidos')
.pauseFor(1000)
.deleteChars(11)

.typeString('Huānyíng')
.pauseFor(1000)
.deleteChars(8)

.typeString('Boas-vindas')
.pauseFor(1000)
.deleteChars(11)

.typeString('Svaagat he')
.pauseFor(1000)
.deleteChars(10)

.typeString('Sbāgata')
.pauseFor(1000)
.deleteChars(11)


.typeString('Bienvenue')

.start()


// FONTION LISTE "ACCORDION"
function Accordion(a,b) {
    b = b||null;
    if (a.nodeType===Node.ELEMENT_NODE) {
        a=[a];
    }
    
    var c=a.length, d, x='accordion', y='active', e=function(a) { 
        a.classList.add(x+'-binded');
        var h=a.querySelector('.'+x+'-head'), i=a.querySelector('.'+x+'-inner'), c=a.querySelector('.'+x+'-content');
        var z=function(a,i,f) {
            i.style.height = i.offsetHeight + 'px';
            setTimeout(function() {
                i.style.removeProperty('height');
                a.classList.remove(''+x+'-'+y);
                if (typeof f==='function') {
                    f(a)
                }
            },10)
        };
        h.addEventListener('click',function() {
            if (a.matches('.'+x+'-'+y)) {
                z(a,i,b)
            } else {
                a.classList.add(''+x+'-'+y);
                i.style.height = c.offsetHeight + 'px';
                if (typeof b==='function') {
                    b(a)
                }
            }
            var p=a.parentNode, q=p.parentNode, r=q.querySelectorAll('.'+x+''), l=r.length,k;
            if (l>0) {
                for (k=0;k<l;k++) {
                    var m=r[k].querySelector('.'+x+'-inner');
                    if (m!==i) {
                        z(r[k],m)
                    }
                }
            }
            while (p!==document.body) {
                if (p.matches('.'+x)) {
                    p.querySelector('.'+x+'-inner').style.height = 'auto'
                }
                p = p.parentNode
            }
        });
        window.addEventListener('resize',function() {
            if (a.matches('.'+x+'-'+y)) {
                i.style.height = c.offsetHeight + 'px'
            }
        })
    };
    if (c>0) { 
        for (d=0;d<c;d++) {
            e(a[d])
        }
    }
}

